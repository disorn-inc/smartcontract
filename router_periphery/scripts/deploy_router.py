from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MainRouter02, network, config, Contract, exceptions
from web3 import Web3

def deploy_router():
    account = get_account()
    # print(config["networks"][network.show_active()]["factory"])
    router = MainRouter02.deploy(
        config["networks"][network.show_active()]["factory"],
        config["networks"][network.show_active()]["wbnb_token"],
        {"from": account},
        publish_source = config["networks"][network.show_active()]["verify"]
    )

def main():
    deploy_router()