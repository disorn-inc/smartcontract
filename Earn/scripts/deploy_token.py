from scripts.helpful_scripts import get_account
from brownie import MainEarn, network, config
from web3 import Web3
import yaml
import json
import os
import shutil

def deploy_token():
    account = get_account()
    main_token_address = "0xbbae870d02dbeDac0EA683C905ef7Ee254D70800"
    earn_token = MainEarn.deploy(main_token_address,{"from": account},publish_source=config["networks"][network.show_active()]["verify"])

def main():
    deploy_token()