from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MainEarn, network, config, Contract, exceptions
from web3 import Web3
import yaml
import json
import os
import shutil

def tranfer_owner():
    account = get_account()
    master_chef_proxy_address = "0x4BFb145C5aEf4D5C2eb604b23cc81446948447f9"
    earn_address = "0x1F322998551C198126001CC8485EF2A9d3e3F114"
    
    earn = Contract.from_abi("MainEarn", earn_address, MainEarn.abi)
    tranfer = earn.transferOwnership(master_chef_proxy_address, {"from": account})
    tranfer.wait(1)
    
def main():
    tranfer_owner()