from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MasterChef, network, config, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions
from web3 import Web3
import yaml
import json
import os
import shutil


KEPT_BALANCE = Web3.toWei(100, "ether")
main_per_block = Web3.toWei(3, "ether")
startblock = 14445000
staking_per = 857000
dev_per = 90000
ref_per = 43000
safu_per = 10000


def deploy_token_farm_and_mock_token(update_front_end=False):
    account = get_account()
    main_token_address = "0xbbae870d02dbeDac0EA683C905ef7Ee254D70800"
    earn_token_address = "0x1F322998551C198126001CC8485EF2A9d3e3F114"
    sec_address_ref = "0xdb01cE9FdC4f1f1ad42DFfef22C2d4D92c91D4Bc"
    thr_address_safu = "0x1125Ffa152F6c1b09a5502d80e971fD3A2F1843B"
    master = MasterChef.deploy( 
        {"from": account},
        publish_source=config["networks"][network.show_active()]["verify"]
    )
    print("token_farm deployed")
    print(master)
    
    proxy_admin = ProxyAdmin.deploy({"from": account}, publish_source = True
                                    )
    print("proxy admin deployed")
    # initializer = token_farm.initialize 
    farm_encoded_initializer_function = encode_function_data()
    
    proxy = TransparentUpgradeableProxy.deploy(
        master.address,
        proxy_admin.address,
        farm_encoded_initializer_function,
        {"from": account, "gas_limit": 1000000}, publish_source = True
    )
    print("proxy deployed")
    
    # trafer_own = token_farm.transferOwnership(proxy.address, {"from": account})
    # trafer_own.wait(1)
    # print("owner tranfer")
    
    stake = Contract.from_abi("MasterChef", proxy.address, MasterChef.abi)
    
    #### Test ####
    init_px = stake.initialize(
        main_token_address, earn_token_address, account, sec_address_ref, thr_address_safu, main_per_block, startblock, staking_per, dev_per, ref_per, safu_per, {"from": account}
        )
    init_px.wait(1)
    
    # own, send, res = stake.checkOwner({"from": account})
    # # test_const.wait(1)
    # print(f"own: {own}, send: {send}, res: {res}")
    
    # mock_token, weth_token, fau_token/dai
    # weth_token = config[""]
    # weth_token = get_contract("weth_token")
    # fau_token = get_contract("fau_token")
    # dict_of_allowed_tokens = {
    #     mock_token: get_contract("dai_usd_price_feed"),
    #     fau_token: get_contract("dai_usd_price_feed"),
    #     weth_token: get_contract("eth_usd_price_feed") 
    # }
    # add_allowed_tokens(stake, dict_of_allowed_tokens, account)
    # if update_front_end:
    #     update_front_end()
    # return stake
    
    
def add_allowed_tokens(proxy_farm, dict_of_allowed_tokens, account):
    for token in dict_of_allowed_tokens:
        add_tx = proxy_farm.AddAllowedTokens(token.address, {"from": account, "gas_limit": 1000000, "allow_revert":True})
        add_tx.wait(1)
        set_tx = proxy_farm.setPriceFeedContract(token.address,
                                                dict_of_allowed_tokens[token],
                                                {"from": account, "gas_limit": 1000000, "allow_revert":True}
        )
        set_tx.wait(1)
    return proxy_farm

def update_front_end():
    copy_folders_to_front_end(src="./build", dest="./front_end/src/chain-info")
    with open("brownie-config.yaml", "r") as brownie_config:
        config_dict = yaml.load(brownie_config, Loader=yaml.FullLoader)
        with open("./brownie-config.json", "w") as brownie_config_json:
            json.dump(config_dict, brownie_config_json)
    print("front end update")
    
    
def copy_folders_to_front_end(src, dest):
    if os.path.exists(dest):
        shutil.rmtree(dest)
    shutil.copytree(src, dest)


def main():
    deploy_token_farm_and_mock_token(update_front_end=False)