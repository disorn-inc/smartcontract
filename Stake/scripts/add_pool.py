from scripts.constant import LP_MAIN_BNB, LP_MAIN_BNB_PLEARN, LP_MAIN_BUSD, MASTERCHEF_PROXY_ADDRESS
from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MasterChef, network, config, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions
from web3 import Web3

def add_pool_masterchef():
    account = get_account()
    master_chef_address = MASTERCHEF_PROXY_ADDRESS
    lp_main_bnb = LP_MAIN_BNB_PLEARN
    masterchef = Contract.from_abi("MasterChef", master_chef_address, MasterChef.abi)
    add_tx = masterchef.add(100, lp_main_bnb, True, {"from": account})
    add_tx.wait(1)

def main():
    add_pool_masterchef()