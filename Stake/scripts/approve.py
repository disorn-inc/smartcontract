from asyncio.proactor_events import constants

from eth_typing import HexStr
from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MasterChef, network, config, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions, BEP20
from web3 import Web3, constants
from eth_utils import to_int

MAX_UINT256 = constants.MAX_INT

def approve_bep20():
    account = get_account()
    main_token_address = "0xbbae870d02dbeDac0EA683C905ef7Ee254D70800"
    master_chef_proxy_address = "0x4BFb145C5aEf4D5C2eb604b23cc81446948447f9"
    main = Contract.from_abi("BEP20", main_token_address, BEP20.abi)
    # approve =  main.approve()
    max_uint256 = to_int(hexstr =  MAX_UINT256)
    approve =  main.approve(master_chef_proxy_address, max_uint256, {"from": account})
    approve.wait(1)
    
def main():
    approve_bep20()