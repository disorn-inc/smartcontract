from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MasterChef, network, config, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions
from web3 import Web3
import yaml
import json
import os
import shutil


KEPT_BALANCE = Web3.toWei(100, "ether")
main_per_block = Web3.toWei(3, "ether")
startblock = 14445000
staking_per = 857000
dev_per = 90000
ref_per = 43000
safu_per = 10000

def initial(update_front_end=False):
    account = get_account()
    main_token_address = "0xbbae870d02dbeDac0EA683C905ef7Ee254D70800"
    earn_token_address = "0x1F322998551C198126001CC8485EF2A9d3e3F114"
    sec_address_ref = "0xdb01cE9FdC4f1f1ad42DFfef22C2d4D92c91D4Bc"
    thr_address_safu = "0x1125Ffa152F6c1b09a5502d80e971fD3A2F1843B"
    proxy_address = "0x4BFb145C5aEf4D5C2eb604b23cc81446948447f9"
    
    stake = Contract.from_abi("MasterChef", proxy_address, MasterChef.abi)
    
    init_px = stake.initialize(
        main_token_address, earn_token_address, account, sec_address_ref, thr_address_safu, main_per_block, startblock, staking_per, dev_per, ref_per, safu_per, {"from": account}
        )
    init_px.wait(1)
    
def main():
    initial()