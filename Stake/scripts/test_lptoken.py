from scripts.constant import LP_MAIN_BNB, LP_MAIN_BNB_PLEARN, LP_MAIN_BUSD, MASTERCHEF_PROXY_ADDRESS
from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MasterChef, network, config, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions, BEP20
from web3 import Web3

def allow_lp():
    account = get_account()
    lp_main_bnb = LP_MAIN_BNB
    lp = Contract.from_abi("lp", lp_main_bnb, BEP20.abi)
    allow_tx = lp.allowance(account, MASTERCHEF_PROXY_ADDRESS)
    print(allow_tx)
    
def balace_of():
    account = get_account()
    lp_main_bnb = LP_MAIN_BNB
    lp = Contract.from_abi("lp", lp_main_bnb, BEP20.abi)
    bal_of_tx = lp.balanceOf(account)
    print(bal_of_tx)

def main():
    allow_lp()