from scripts.helpful_scripts import get_account, encode_function_data
from brownie import Multicall2, network, config, Contract, exceptions
from web3 import Web3

def deploy_multicall():
    account = get_account()
    multicall = Multicall2.deploy(
        {"from": account},
        publish_source=config["networks"][network.show_active()]["verify"])

def main():
    deploy_multicall()