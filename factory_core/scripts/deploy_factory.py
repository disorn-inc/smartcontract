from scripts.helpful_scripts import get_account, encode_function_data
from brownie import MainFactory, network, config, ProxyAdmin, TransparentUpgradeableProxy, Contract, exceptions
from web3 import Web3

def deploy_factory():
    account = get_account()
    factory = MainFactory.deploy({"from": account},
        publish_source=config["networks"][network.show_active()]["verify"]
    )
    # factory.wait(1)
    proxy_admin = ProxyAdmin.deploy({"from": account},
                                    publish_source = config["networks"][network.show_active()]["verify"])
    
    factory_encoded_initializer_function = encode_function_data()
    
    proxy = TransparentUpgradeableProxy.deploy(
        factory.address,
        proxy_admin.address,
        factory_encoded_initializer_function,
        {"from": account, "gas_limit": 1000000}, publish_source = config["networks"][network.show_active()]["verify"]
    )
    print("proxy deployed")
    
    fac_proxy = Contract.from_abi("MainFactory", proxy.address, MainFactory.abi)
    
    fac_init = fac_proxy.initialize(account, {"from": account})
    fac_init.wait(1)
    
def main():
    deploy_factory()