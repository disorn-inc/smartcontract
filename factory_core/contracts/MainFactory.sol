pragma solidity >=0.8.0;

import "@openzeppelin-upgradeable/contracts/proxy/utils/Initializable.sol";
import './interfaces/IPlearnFactory.sol';
import './MainPair.sol';

contract MainFactory is Initializable, IPlearnFactory {
    address public override feeTo;
    address public override feeToSetter;
    bytes32 public constant override INIT_CODE_HASH = keccak256(abi.encodePacked(type(MainPair).creationCode));

    mapping(address => mapping(address => address)) public override getPair;
    address[] public override allPairs;

    function initialize(address _feeToSetter) public initializer {
        feeToSetter = _feeToSetter;
    }

    function allPairsLength() external view override returns (uint) {
        return allPairs.length;
    }

    function createPair(address tokenA, address tokenB) external override returns (address pair) {
        require(tokenA != tokenB, 'Main: IDENTICAL_ADDRESSES');
        (address token0, address token1) = tokenA < tokenB ? (tokenA, tokenB) : (tokenB, tokenA);
        require(token0 != address(0), 'Main: ZERO_ADDRESS');
        require(getPair[token0][token1] == address(0), 'Main: PAIR_EXISTS'); // single check is sufficient
        bytes memory bytecode = type(MainPair).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(token0, token1));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        IPlearnPair(pair).initialize(token0, token1);
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair; // populate mapping in the reverse direction
        allPairs.push(pair);
        emit PairCreated(token0, token1, pair, allPairs.length);
    }

    function setFeeTo(address _feeTo) external override {
        require(msg.sender == feeToSetter, 'Main: FORBIDDEN');
        feeTo = _feeTo;
    }

    function setFeeToSetter(address _feeToSetter) external override {
        require(msg.sender == feeToSetter, 'Main: FORBIDDEN');
        feeToSetter = _feeToSetter;
    }

    function setDevFee(address _pair, uint8 _devFee) external override {
        require(msg.sender == feeToSetter, 'Main: FORBIDDEN');
        require(_devFee > 0, 'Main: FORBIDDEN_FEE');
        MainPair(_pair).setDevFee(_devFee);
    }
    
    function setSwapFee(address _pair, uint32 _swapFee) external override {
        require(msg.sender == feeToSetter, 'Main: FORBIDDEN');
        MainPair(_pair).setSwapFee(_swapFee);
    }
}
