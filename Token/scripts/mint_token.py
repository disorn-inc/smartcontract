from scripts.helpful_scripts import get_account
from brownie import MainToken, network, config, Contract
from web3 import Web3

MINT_BALANCE = Web3.toWei(10000000, "ether")

def mint_token():
    account = get_account()
    main_token_address = "0xbbae870d02dbeDac0EA683C905ef7Ee254D70800"
    main_token = Contract.from_abi("MainToken", main_token_address, MainToken.abi)
    mint = main_token.mint(account, MINT_BALANCE, {"from": account})
    mint.wait(1)
    total_supply = main_token.totalSupply()
    print(total_supply)

def main():
    mint_token()