from scripts.helpful_scripts import get_account
from brownie import MainToken, network, config
from web3 import Web3
import yaml
import json
import os
import shutil

def deploy_token():
    account = get_account()
    main_token = MainToken.deploy({"from": account},publish_source=config["networks"][network.show_active()]["verify"])
    add_minter = main_token.addMinter(account, {"from": account})
    add_minter.wait(1)

def main():
    deploy_token()