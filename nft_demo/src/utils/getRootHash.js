const { MerkleTree } = require("merkletreejs");
const keccak256 = require("keccak256");

const getLeafNode = (whitelistAddresses) => {
    const leaf = whitelistAddresses.map((addr) => {keccak256(addr)});
    return leaf;
}

const markleTree = (leafNodes) => {
    const markleTree = new MerkleTree(leafNodes, keccak256, { sortPairs: true });
    return markleTree;
}

const getRootHash = (markleTree) => {
    return markleTree.getRootHash();
}

const hashMinter = (address) => {
    return keccak256(address);
}

const getHexProof = (markleTree, hashedMinter) => {
    const hexProof = markleTree.getHexProof(hashedMinter);
    return hexProof;
}

export {getLeafNode, markleTree, getRootHash, hashMinter, getHexProof};