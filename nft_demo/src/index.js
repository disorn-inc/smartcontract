// import { getLeafNode, markleTree, getRootHash } from "./utils/getRootHash";
const whitelistAddresses = require("./whitelist.json");

const otherAddress = "0x0F10BF8A1642146A8081bfdD2C456995f11882d5"

console.log(whitelistAddresses);

const { MerkleTree } = require("merkletreejs");
const keccak256 = require("keccak256");

const getLeafNode = (whitelistAddresses) => {
    // console.log(whitelistAddresses);
    const leaf = whitelistAddresses.map((addr) => keccak256(addr));
    // console.log(leaf);
    return leaf;
}

const getmarkleTree = (leafNodes) => {
    const markleTree = new MerkleTree(leafNodes, keccak256, { sortPairs: true });
    return markleTree;
}

const getRootHash = (markleTree) => {
    return markleTree.getRoot();
}

const hashMinter = (address) => {
    return keccak256(address);
}

const getHexProof = (markleTree, hashedMinter) => {
    const hexProof = markleTree.getHexProof(hashedMinter);
    return hexProof;
}

const leafNodes = getLeafNode(whitelistAddresses);
console.log(leafNodes);

const markleTrees = getmarkleTree(leafNodes);

const rootHash = getRootHash(markleTrees);
console.log(" => whitelist merkle tree", markleTrees.toString());
console.log(" ==> Root hash", rootHash.toString("hex"));

// const minterHash = hashMinter(whitelistAddresses[0]);
const minterHash = hashMinter(otherAddress);
console.log("=> hash Minter", minterHash);

const hexProof = getHexProof(markleTrees, minterHash);
console.log(" => hexProof", hexProof);