// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@erc721a/contracts/ERC721A.sol";

contract Erc721aTest is ERC721A {

    mapping (uint256 => string) private _tokenURIs;

    constructor() ERC721A("Test 721A", "T721A") {}

    function mint(uint256 quantity) external payable {
        // _safeMint's second argument now takes in a quantity, not a tokenId.
        _safeMint(msg.sender, quantity);
    }

    function _setTokenUri(uint256 tokenId, string memory tokenURI) private {
         _tokenURIs[tokenId] = tokenURI; 
    } 

    function setTokenUri(uint256 tokenId ,string memory tokenURI) public {
        require(_exists(tokenId), "GoldigNFT: token id not exits");
        _setTokenUri(tokenId, tokenURI);
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        if (!_exists(tokenId)) revert URIQueryForNonexistentToken();
        return _tokenURIs[tokenId];
    }
}