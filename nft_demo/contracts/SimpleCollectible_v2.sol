// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract SimpleCollectible_v2 is ERC721URIStorage {

    uint256 public tokenCounter;

    constructor() public ERC721 ("Doggie", "DOG"){
        tokenCounter = 0;
    }

    function createCollectible() public returns (uint256) {
        uint256 newTokenId = tokenCounter;
        _safeMint(msg.sender, newTokenId);
        tokenCounter += 1;
        return newTokenId;
    }

    function setTokenURI(uint256 tokenId ,string memory tokenURI) public {
        _setTokenURI(tokenId, tokenURI);
    }

}