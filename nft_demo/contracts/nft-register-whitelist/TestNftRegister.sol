// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import '@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol';
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

contract TestNftRegister is ERC721Enumerable, Ownable, Pausable {
    uint256 public tokenCounter;

    uint8 maxWhitelistMint = 2;

    bytes32 whitelistRootHash;

    string baseUri;

    mapping(address => uint256) claimedWhitelist;

    mapping(uint256 => string) private _tokenURIs;

    constructor() public ERC721 ("Markle", "MAR"){
        tokenCounter = 0;
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), "ERC721URIStorage: URI query for nonexistent token");

        string memory _tokenURI = _tokenURIs[tokenId];
        string memory base = _baseURI();

        // If there is no base URI, return the token URI.
        if (bytes(base).length == 0) {
            return _tokenURI;
        }
        // If both are set, concatenate the baseURI and tokenURI (via abi.encodePacked).
        if (bytes(_tokenURI).length > 0) {
            return string(abi.encodePacked(base, _tokenURI));
        }

        return super.tokenURI(tokenId);
    }

    function _setTokenURI(uint256 tokenId, string memory _tokenURI) internal virtual {
        require(_exists(tokenId), "ERC721URIStorage: URI set of nonexistent token");
        _tokenURIs[tokenId] = _tokenURI;
    }

    function _setBaseURI(string memory _tokenURI) internal virtual {
        baseUri = _tokenURI;
    }

    function _burn(uint256 tokenId) internal virtual override {
        super._burn(tokenId);

        if (bytes(_tokenURIs[tokenId]).length != 0) {
            delete _tokenURIs[tokenId];
        }
    }

    function setWhitelistRootHash(bytes32 rootHash) public onlyOwner {
        whitelistRootHash = rootHash;
    }

    function userClaim(bytes32[] memory proofs, uint256 tokenId) public whenNotPaused{
        require(proofs.length > 0, 'no proof included');
        bytes32 leafNode = keccak256(abi.encodePacked(msg.sender));
        require(MerkleProof.verify(proofs, whitelistRootHash, leafNode), 'You are not in white list');
        require(claimedWhitelist[msg.sender] < maxWhitelistMint, 'Your whitelist is complete');
        _safeMint(msg.sender, tokenId);
        _setTokenURI(tokenId, baseUri);
        claimedWhitelist[msg.sender] += 1;
    }

    function puaseContract() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpuaseContract() external onlyOwner whenPaused {
        _unpause();
    }
}