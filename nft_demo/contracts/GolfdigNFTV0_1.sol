// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

contract GolfdigNFTV0_1 is ERC721URIStorage, Ownable, Pausable {

    using EnumerableSet for EnumerableSet.AddressSet;

    uint256 public tokenCounter;

    EnumerableSet.AddressSet private _minters;

    uint256 private constant _TIMELOCK = 5 minutes;
    uint256 private constant _LIMIT = 5;

    struct UserInfo {
        uint256 mintLimit;
        uint256 expireTime;
        uint256 permissionLevel;
    }

    mapping(address => UserInfo) public userInfo;

    constructor() public ERC721 ("GolfdigNFT", "GOLF"){
        tokenCounter = 0;
    }

    function createCollectibleLevel1(string memory tokenURI) public onlyMinter whenNotPaused returns (uint256) {
        UserInfo storage user = userInfo[msg.sender];
        require(user.expireTime >= block.timestamp, "GoldigNFT: You're time has expired");
        require(user.mintLimit >= _LIMIT);
        require(user.permissionLevel > 0);
        uint256 newTokenId = tokenCounter;
        _safeMint(msg.sender, newTokenId);
        _setTokenURI(newTokenId, tokenURI);
        tokenCounter += 1;
        return newTokenId;
    }

    function addPermision(address minter, uint level) public onlyOwner {
        UserInfo storage user = userInfo[minter];
        user.permissionLevel = level;
        user.expireTime = block.timestamp + _TIMELOCK;
        user.mintLimit = _LIMIT;
    }

    function addMinter(address _addMinter) public onlyOwner returns (bool) {
        require(
            _addMinter != address(0),
            "GoldigNFT: _addMinter is the zero address"
        );
        return EnumerableSet.add(_minters, _addMinter);
    }

    function delMinter(address _delMinter) public onlyOwner returns (bool) {
        require(
            _delMinter != address(0),
            "GoldigNFT: _delMinter is the zero address"
        );
        return EnumerableSet.remove(_minters, _delMinter);
    }

    function getMinterLength() public view returns (uint256) {
        return EnumerableSet.length(_minters);
    }

    function isMinter(address account) public view returns (bool) {
        return EnumerableSet.contains(_minters, account);
    }

    function getMinter(uint256 _index) public view onlyOwner returns (address) {
        require(_index <= getMinterLength() - 1, "GoldigNFT: index out of bounds");
        return EnumerableSet.at(_minters, _index);
    }

    // modifier for mint function
    modifier onlyMinter() {
        require(isMinter(msg.sender), "caller is not the minter");
        _;
    }

    function puaseContract() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpuaseContract() external onlyOwner whenPaused {
        _unpause();
    }

    function checkTimeLock() public view returns (uint256) {
        return _TIMELOCK;
    }

    function checkTimeNow() public view returns (uint256) {
        return block.timestamp;
    }

    function userTimeRemain(address minter) public view returns (uint256) {
        UserInfo storage user = userInfo[minter];
        uint256 timeNow = block.timestamp;
        if(user.expireTime > timeNow) {
            return user.expireTime - timeNow;
        } else {
            return 0;
        }
    }
}