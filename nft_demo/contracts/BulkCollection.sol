// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

contract BulkCollection is ERC1155 {
    uint256 public constant RARE = 0;
    uint256 public constant UNRARE = 1;

    mapping (uint256 => string) private _tokenURIs;

    constructor() ERC1155("Anything_you_want") {}

    function uri(uint256 tokenId) override public view 
    returns (string memory) { 
        return(_tokenURIs[tokenId]); 
    }

    function _setTokenUri(uint256 tokenId, string memory tokenURI) private {
         _tokenURIs[tokenId] = tokenURI; 
    } 

    function setRareUri(string memory tokenURI) public {
        _setTokenUri(RARE, tokenURI);
    }

    function setUnRareUri(string memory tokenURI) public {
        _setTokenUri(UNRARE, tokenURI);
    }

    function mintRare(uint256 amount) public {
        _mint(msg.sender, RARE, amount, "");
    }

    function mintUnRare(uint256 amount) public {
        _mint(msg.sender, UNRARE, amount, "");
    }
}