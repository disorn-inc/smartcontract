from scripts.helpful_scripts import fund_with_link, get_account, OPENSEA_URL, get_contract
from brownie import AdvanceCollectible ,network, config, Contract
from web3 import Web3


def create_collectible():
    account = get_account()
    advance_col = AdvanceCollectible[-1]
    fund_with_link(advance_col.address,amount= Web3.toWei(0.1, "ether"))
    create_tx = advance_col.createCollectible({"from": account})
    create_tx.wait(1)
    print("Created")

def main():
    create_collectible()