from brownie import TestMerkleTree, config, network
from scripts.helpful_scripts import get_account

ROOT_HASH = '0x6bbf6334418703eb66771ba50e03e6012aa378f1a71e9ef835dac6d4e031be7c'

def set_root():
    account = get_account()
    merkle = TestMerkleTree[-1]
    set_hash_tx = merkle.setWhitelistRootHash(ROOT_HASH, {"from": account})
    set_hash_tx.wait(1)

def main():
    set_root()