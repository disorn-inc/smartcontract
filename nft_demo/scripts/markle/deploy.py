from brownie import TestMerkleTree, config, network
from scripts.helpful_scripts import get_account

def deploy_merkle():
    account = get_account()
    merkle = TestMerkleTree.deploy(
        {"from": account}, 
        publish_source=config["networks"][network.show_active()]["verify"]
    )
        
def main():
    deploy_merkle()