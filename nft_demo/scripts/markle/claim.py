from brownie import TestMerkleTree, config, network
from scripts.markle.hexproof import ADDRESS_1
from scripts.helpful_scripts import get_account

def claim():
    account = get_account()
    merkle = TestMerkleTree[-1]
    hex_proof = ADDRESS_1
    claim_tx = merkle.userClaim(hex_proof, 1, {"from": account})
    claim_tx.wait(1)

def main():
    claim()