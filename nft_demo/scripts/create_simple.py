from scripts.helpful_scripts import fund_with_link, get_account, OPENSEA_URL, get_contract
from brownie import SimpleCollectible ,network, config, Contract
from web3 import Web3

sample_token_uri = "https://ipfs.io/ipfs/QmcThh7p4MA2rQpvjbxApBbyNL7nUScu7ZprEE2iiX9t1j?filename=1-SHIBA_INU.json"

def create_collectible():
    account = get_account()
    simple_col = SimpleCollectible[-1]
    tx = simple_col.createCollectible(sample_token_uri, {"from": account})
    tx.wait(1)
    print("Created")

def main():
    create_collectible()