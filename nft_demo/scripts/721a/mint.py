from brownie import Erc721aTest, config, network
from scripts.helpful_scripts import get_account

def mint():
    account = get_account()
    erc721a = Erc721aTest[-1]
    mint_tx = erc721a.mint(3, {"from": account})
    mint_tx.wait(1)

def main():
    mint()