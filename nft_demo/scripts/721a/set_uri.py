from brownie import Erc721aTest, config, network
from scripts.helpful_scripts import get_account

URI = "https://ipfs.io/ipfs/QmW4qPbedTL7DS3AgdzMnrw7YKYZB2miLAywJBNZe97kTz?filename=0-ST_BERNARD.json"

def set_uri():
    account = get_account()
    erc721a = Erc721aTest[-1]
    mint_tx = erc721a.setTokenUri(0, URI, {"from": account})
    mint_tx.wait(1)

def main():
    set_uri()