from brownie import Erc721aTest, config, network
from scripts.helpful_scripts import get_account

def deploy():
    account = get_account()
    erc721a = Erc721aTest.deploy(
        {"from": account}, 
        publish_source=config["networks"][network.show_active()]["verify"]
    )

def main():
    deploy()