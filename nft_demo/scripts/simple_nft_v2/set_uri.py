from scripts.helpful_scripts import get_account
from brownie import SimpleCollectible_v2 ,network, config, Contract

sample_token_uri = "https://ipfs.io/ipfs/Qmd9MCGtdVz2miNumBHDbvj8bigSgTwnr4SbyH6DNnpWdt?filename=0-PUG.json"
OPENSEA_URL = "https://testnets.opensea.io/assets/{}/{}"

def set_uri():
    account = get_account()
    simple_col = SimpleCollectible_v2[-1]
    set_tx = simple_col.setTokenURI(0, sample_token_uri, {"from": account})
    set_tx.wait(1)

def main():
    set_uri()