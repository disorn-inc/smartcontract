from brownie import BulkCollection, config, network
from scripts.helpful_scripts import get_account

SHIBA_URI = "https://ipfs.io/ipfs/QmcThh7p4MA2rQpvjbxApBbyNL7nUScu7ZprEE2iiX9t1j?filename=1-SHIBA_INU.json"

def set_rare_uri():
    account = get_account()
    bulk = BulkCollection[-1]
    set_uri_tx = bulk.setRareUri(SHIBA_URI, {"from": account})
    set_uri_tx.wait(1)

def main():
    set_rare_uri()