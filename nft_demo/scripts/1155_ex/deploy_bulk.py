from brownie import BulkCollection, config, network
from scripts.helpful_scripts import get_account

def deploy_bulk():
    account = get_account()
    bulk = BulkCollection.deploy(
        {"from": account}, 
        publish_source=config["networks"][network.show_active()]["verify"]
    )
        
def main():
    deploy_bulk()