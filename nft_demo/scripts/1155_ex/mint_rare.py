from brownie import BulkCollection, config, network
from scripts.helpful_scripts import get_account

def mint_rare():
    account = get_account()
    bulk = BulkCollection[-1]
    mint_tx = bulk.mintRare(1,{"from": account})
    mint_tx.wait(1)

def main():
    mint_rare()