import json
from scripts.helpful_scripts import fund_with_link, get_account, OPENSEA_URL, get_breed, get_contract
from brownie import AdvanceCollectible ,network, config, Contract
from web3 import Web3
from metadata.sample_metadata import metadata_template
from pathlib import Path
import requests

def create_metadata():
    advance_col = AdvanceCollectible[-1]
    number_of_advance_col = advance_col.tokenCounter()
    print(f"You have created {number_of_advance_col} collectibles")
    for token_id in range(number_of_advance_col):
        breed = get_breed(advance_col.tokenIdToBreed(token_id))
        metadata_file_name = (
            f"./metadata/{network.show_active()}2/{token_id}-{breed}.json"
        )
        # map_file_name = (
        #     f"./metadata/{network.show_active()}/map.json"
        # )
        collectible_metadata = metadata_template
        if Path(metadata_file_name).exists():
            print(f"{metadata_file_name} already exists! Delete it to overwrite")
        else:
            print(f"Creating Metadata file: {metadata_file_name}")
            collectible_metadata["name"] = breed
            collectible_metadata["description"] = f"An adorable {breed} pup!"
            image_path = "./img/" + breed.lower().replace("_", "-") + ".png"
            image_uri = upload_to_ipfs(image_path)
            collectible_metadata["image"] = image_uri
            print(collectible_metadata)
            with open(metadata_file_name, "w") as file:
                json.dump(collectible_metadata, file)
            # json_uri = upload_to_ipfs(metadata_file_name)
            upload_to_ipfs(metadata_file_name)
            
            
def upload_to_ipfs(filepath):
    with Path(filepath).open("rb") as fp:
        image_binary = fp.read()
        ipfs_url = "http://127.0.0.1:5001"
        endpoint = "/api/v0/add"
        response = requests.post(ipfs_url + endpoint, files={"file": image_binary})
        ipfs_hash = response.json()["Hash"]
            #   "./img/0-PUG.png" -> 0-PUG.png
        filename = filepath.split("/")[-1:][0]
        image_uri = f"https://ipfs.io/ipfs/{ipfs_hash}?filename={filename}"
        print(image_uri)
        return image_uri

def main():
    create_metadata()