from scripts.helpful_scripts import fund_with_link, get_account, OPENSEA_URL, get_contract
from brownie import AdvanceCollectible ,network, config, Contract

sample_token_uri = "https://ipfs.io/ipfs/Qmd9MCGtdVz2miNumBHDbvj8bigSgTwnr4SbyH6DNnpWdt?filename=0-PUG.json"
OPENSEA_URL = "https://testnets.opensea.io/assets/{}/{}"

def deploy_nft():
    account = get_account()
    advance_col = AdvanceCollectible.deploy(
        get_contract("vrf"),
        get_contract("link_token"),
        config["networks"][network.show_active()]["keyhash"],
        config["networks"][network.show_active()]["fee"],
        {"from": account},
        publish_source=config["networks"][network.show_active()]["verify"]
    )
    fund_with_link(advance_col.address)
    create_tx = advance_col.createCollectible({"from": account})
    create_tx.wait(1)
    print("Created")

def main():
    deploy_nft()