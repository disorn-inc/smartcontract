from scripts.helpful_scripts import get_account
from brownie import SimpleCollectible ,network, config, Contract

sample_token_uri = "https://ipfs.io/ipfs/Qmd9MCGtdVz2miNumBHDbvj8bigSgTwnr4SbyH6DNnpWdt?filename=0-PUG.json"
OPENSEA_URL = "https://testnets.opensea.io/assets/{}/{}"

def deploy_nft():
    account = get_account()
    simple_col = SimpleCollectible.deploy({"from": account},publish_source=config["networks"][network.show_active()]["verify"])
    tx = simple_col.createCollectible(sample_token_uri, {"from": account})
    tx.wait(1)
    print(f"you link at {OPENSEA_URL.format(simple_col.address, simple_col.tokenCounter() - 1)}")

def main():
    deploy_nft()